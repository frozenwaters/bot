import discord
import sqlite3
import yaml

from discord.ext import commands

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)

class Events(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        client = self.client

        activity = discord.Activity(name='the lake freeze over!', type=discord.ActivityType.watching)
        status = discord.Status.online

        if bot['dev']:
            activity = discord.Game(name='with the devs!')

        await client.change_presence(status=status, activity=activity)

        print('\033[36m~~~~~~~~~~~~~')
        print('\033[36mLogged in as - ')
        print(f'\033[36mName: {client.user.name}')
        print(f'\033[36mId: {client.user.id}')
        print('\033[36m~~~~~~~~~~~~~')
        print('\n\033[36m(Pterodactyl Bot Online)')

        if bot['dev']:
            print('\n\033[93mDEV MODE ENABLED')

    @commands.Cog.listener()
    async def on_member_join(self, member):
        client = self.client

        if bot['dev']:
            return

        channel = discord.utils.get(member.guild.channels, id=528332578101657621)
        embed = discord.Embed(
            title=f'Welcome to the server {member.name}!',
            description=f'Please make sure to read <#623596187907260454>!\n\n• To apply for any roles, run `{config["prefix"]}apply`\n• To open a support ticket, run `{config["prefix"]}new`\n• For more commands, run `{config["prefix"]}help`',
            colour=config['embed']['colour']
        )
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])

        embed.set_thumbnail(url=member.avatar_url)
        await channel.send(member.mention, embed=embed)

        role = discord.utils.get(member.guild.roles, id=528009722679263245)
        await member.add_roles(role)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound):
            return

        elif isinstance(error, commands.CommandOnCooldown):
            await ctx.send('Command is currently on cooldown.')
            return

        elif isinstance(error, commands.BadArgument) or isinstance(error, commands.MissingRequiredArgument):
            embed = discord.Embed(
                description=f'Invalid arguments passed. `{bot["prefix"]}{ctx.command.usage}`',
                colour=config['embed']['colour']
            )
            await ctx.send(embed=embed)

        elif isinstance(error, commands.CheckFailure):
            return

        else:
            raise error


def setup(client):
    client.add_cog(Events(client))
