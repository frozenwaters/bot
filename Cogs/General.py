import inspect
import random

import yaml
import discord
import sqlite3
import time
import requests

from discord.ext import commands
from Utils import permissions

from mcstatus import MinecraftServer

server = MinecraftServer.lookup("play.frozenwaters.net")

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


class General(commands.Cog):
    def __init__(self, client):
        self.client = client

    @permissions.BotAdmin()
    @commands.group(name='say',
                    invoke_without_command=True,
                    usage='say [message/embed]',
                    description='Make the bot say something.')
    async def say(self, context):
        raise commands.MissingRequiredArgument(inspect.Parameter('UsageError', inspect.Parameter.POSITIONAL_ONLY))

    @permissions.BotAdmin()
    @say.command(name='text', description='Make the bot say something in message format.',
                 usage='message text [text]')
    async def text_say(self, context, *, content):
        await context.message.delete()
        await context.send(content)

    @permissions.BotAdmin()
    @say.command(name='embed', description='Make the bot say something in message format.',
                 usage='message embed [embed title;embed content]')
    async def embed_say(self, context, *, content):
        content = content.split(';')

        if len(content) != 2:
            raise commands.MissingRequiredArgument(inspect.Parameter('UsageError', inspect.Parameter.POSITIONAL_ONLY))

        embed = discord.Embed(
            title=content[0],
            description=content[1],
            colour=config['embed']['colour']
        )
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])

        await context.message.delete()
        await context.send(embed=embed)

    @permissions.BotAdmin()
    @commands.command(name='react',
                      description='React to a message with a reaction (Must run in channel)',
                      usage='react [message id] [reaction]')
    async def react(self, context, messageId, reaction):
        message = await context.message.channel.fetch_message(messageId)
        await message.add_reaction(reaction)

    @commands.command(name='ping',
                      description='Gives the bot latency in milliseconds.',
                      usage='ping')
    async def ping(self, context):
        client = self.client
        embed = discord.Embed(
            description='🏓 Pong!',
            colour=config['embed']['colour']
        )

        before = time.monotonic()
        message = await context.send(embed=embed)
        pingValue = (time.monotonic() - before) * 1000

        embed.description = f'🏓 Pong! Latency: `{int(pingValue)}ms`'
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])
        await message.edit(embed=embed)

    @commands.cooldown(1, 10, commands.BucketType.guild)
    @commands.command(name='status',
                      description='Gets information about the network',
                      usage='status')
    async def status(self, context):
        client = self.client

        query = server.query()
        status = server.status()

        if query.players.online == 0:
            playerList = 'None'

        elif query.players.online < 5:
            playerList = ", ".join(query.players.names)

        else:
            playerList = ", ".join(random.sample(query.players.names, 5))

            if query.players.online > 5:
                playerList += f' and {query.players.online - 5} more...'

        embed = discord.Embed(
            title='Frozenwaters Status',
            colour=config['embed']['colour'],
            description=f'The server replied in {round(status.latency) + random.randint(1, 5)}ms.\n\nThere is currently {query.players.online} players online:\n `{playerList}`\n\n** **'
        )

        count = 0

        for i in config['servers']['public']:
            ping = requests.get(f'http://95.217.39.33:8790/data/{config["servers"]["public"][i]["bungeeName"]}').json()

            if ping[1][-1] == -1:
                emoji = '🟥'
                status = 'offline'
                players = 'N/A'
            else:
                emoji = '🟩'
                status = 'online'
                players = ping[1][-1]

            count += 1
            embed.add_field(name=f'{emoji} {config["servers"]["public"][i]["displayName"]}',
                            value=f'**Status:** `{status}`\n**Players:** `{players}`')

        for i in config['servers']['private']:
            ping = requests.get(f'http://95.217.39.33:8790/data/{config["servers"]["private"][i]["bungeeName"]}').json()

            if ping[1][-1] == -1:
                emoji = '🟥'
                status = 'offline'
                players = 'N/A'
            else:
                emoji = '🟧'
                status = 'private'
                players = ping[1][-1]
            count += 1
            embed.add_field(name=f'{emoji} {config["servers"]["private"][i]["displayName"]}', value=f'**Status:** `{status}`\n**Players:** `{players}`')

        while count % 3 != 0:
            embed.add_field(name='\u200b', value='\u200b')
            count += 1

        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])
        await context.send(embed=embed)

    @commands.command(name='ip',
                      description='Gives the server IP',
                      usage='ip')
    async def ip(self, context):
        client = self.client

        embed = discord.Embed(
            title="Frozenwaters IP",
            description='The server IP is `play.frozenwaters.net`'
        )
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])

        await context.send(embed=embed)

 #    @commands.command(name='help',
 #                      description='Gives the help menu',
 #                      usage='help')
 #    async def helpCommand(self, context, *, command: str = None):
 #        client = self.client
 #
 #        if not command:
 #            embed = discord.Embed(
 #                title="Help Menu",
 #                description='These are all the commands which can be run with the Frozenwaters Bot.',
 #                colour=config['embed']['colour'],
 #            )
 #            embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])
 #
 #            # --------------[ Utilities ]-------------- #
 #            string = f'`{config["prefix"]}{client.get_command("help").usage}` ● {client.get_command("help").description}\n' \
 #                     f'`{config["prefix"]}{client.get_command("ping").usage}` ● {client.get_command("ping").description}\n' \
 #                     f'`{config["prefix"]}{client.get_command("ip").usage}` ● {client.get_command("ip").description}\n' \
 #                     f'`{config["prefix"]}{client.get_command("status").usage}` ● {client.get_command("status").description}\n' \
 # \
 #            embed.add_field(name='Utilities', value=string, inline=False)
 #            # --------------[ Utilities ]-------------- #
 #
 #            # --------------[ Suggestions ]-------------- #
 #            string = f'`{config["prefix"]}{client.get_command("suggest").usage}` ● {client.get_command("suggest").description}\n'
 #
 #            embed.add_field(name='Suggestions', value=string, inline=False)
 #            # --------------[ Suggestions ]-------------- #
 #
 #            # --------------[ Bot Admin ]-------------- #
 #            if permissions.BotAdminCheck(context):
 #                string = f'`{config["prefix"]}{client.get_command("cog").usage}` ● {client.get_command("cog").description}\n' \
 #                         f'`{config["prefix"]}{client.get_command("say").usage}` ● {client.get_command("say").description}\n' \
 #                         f'`{config["prefix"]}{client.get_command("react").usage}` ● {client.get_command("react").description}\n'
 #
 #                embed.add_field(name='Bot Admin', value=string, inline=False)
 #            # --------------[ Bot Admin ]-------------- #
 #
 #            await context.send(embed=embed)
 #
 #        else:
 #            text = None
 #            try:
 #                text = f'Usage: `{client.get_command(command).usage}`\nDescription: `{client.get_command(command).description}`'
 #            except:
 #                text = 'Command does not exist.'
 #
 #            embed = discord.Embed(
 #                title=f"Frozenwaters - {command.title()} command",
 #                description=text,
 #                colour=config['embed']['colour']
 #            )
 #            embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])
 #            await context.send(embed=embed)


def setup(client):
    client.add_cog(General(client))
