import inspect
import yaml
import discord
import sqlite3
import time
import requests
import random
import string

from discord.ext import commands
from Utils.database import fetch, insert

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


def suggestionId(length):
    characters = string.ascii_lowercase + string.digits
    result = ''.join(random.choice(characters) for i in range(length))
    return result


class Suggestions(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(name='suggest',
                      description='Suggest something for #suggestions',
                      usage='suggest [suggestion]')
    async def suggest(self, context, *, suggestion):
        client = self.client

        while True:
            suggestionid = suggestionId(8)

            thing = fetch("SELECT * FROM {} WHERE {} = '{}'".format('Suggestions', 'id', suggestionid))

            if not thing:
                break

        embed = discord.Embed(
            title=f'Suggestion from {context.message.author.name} - {suggestionid}',
            description=f'{suggestion}',
            colour=config['embed']['colour']
        )
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])

        embed2 = discord.Embed(
            description=f'Successfully sent suggestion in <#623605243187298334>!',
            colour=config['embed']['colour']
        )
        embed2.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])

        channel = client.get_channel(config['channels']['suggestions'])
        message = await channel.send(embed=embed)
        await context.send(embed=embed2)

        enabled = client.get_emoji(575019088821092381)
        disabled = client.get_emoji(575019099139342356)

        await message.add_reaction(enabled)
        await message.add_reaction(disabled)

        insert("INSERT INTO Suggestions(id,messageId,authorId,suggestion) VALUES(?,?,?,?)", [suggestionid, context.message.id, context.message.author.id, suggestion])


def setup(client):
    client.add_cog(Suggestions(client))
