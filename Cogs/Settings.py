import discord
import sqlite3
import yaml

from discord.ext import commands

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


class Settings(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        client = self.client

        if bot['dev']:
            return

        if payload.message_id == config['messages']['settings']['message']:
            channel = client.get_channel(config['messages']['settings']['channel'])
            user = payload.member

            if payload.emoji.name == '📰':  # @Alerts
                role = discord.utils.get(channel.guild.roles, id=612588579524313100)
                await user.add_roles(role)

            elif payload.emoji.name == '🎁':  # @Giveaways
                role = discord.utils.get(channel.guild.roles, id=612588732901621761)
                await user.add_roles(role)

            elif payload.emoji.name == '🗒️':  # @Polls
                role = discord.utils.get(channel.guild.roles, id=612588489887973403)
                await user.add_roles(role)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        client = self.client

        if bot['dev']:
            return

        if payload.message_id == config['messages']['settings']['message']:
            channel = client.get_channel(config['messages']['settings']['channel'])
            user = discord.utils.get(channel.guild.members, id=payload.user_id)

            if payload.emoji.name == '📰':  # @Alerts
                role = discord.utils.get(channel.guild.roles, id=612588579524313100)
                await user.remove_roles(role)

            elif payload.emoji.name == '🎁':  # @Giveaways
                role = discord.utils.get(channel.guild.roles, id=612588732901621761)
                await user.remove_roles(role)

            elif payload.emoji.name == '🗒️':  # @Polls
                role = discord.utils.get(channel.guild.roles, id=612588489887973403)
                await user.remove_roles(role)


def setup(client):
    client.add_cog(Settings(client))
