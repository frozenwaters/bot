import inspect
import yaml
import discord
import sqlite3
import time
import requests
import random
import string

from discord.ext import commands
from Utils.database import fetch, insert, update
from Utils.image import generateImage

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


def suggestionId(length):
    characters = string.ascii_lowercase + string.digits
    result = ''.join(random.choice(characters) for i in range(length))
    return result


class Levels(commands.Cog):
    def __init__(self, client):
        self.client = client
        self._cd = commands.CooldownMapping.from_cooldown(1.0, 10.0, commands.BucketType.user)

    @commands.command(name='rank',
                      description='Generate rank card',
                      usage='rank [user]')
    async def rank(self, context, *, user: discord.Member = None):
        user = context.author if not user else user

        await user.avatar.with_format('png').save(f'Assets/Profiles/{user.id}.png')
        result = generateImage(user)

        if result:
            file = discord.File(fp=f'Assets/Cards/{user.id}.png')
            await context.send(file=file)

    @commands.Cog.listener()
    async def on_message(self, message):
        bucket = self._cd.get_bucket(message)
        retry_after = bucket.update_rate_limit()
        if retry_after or message.author.bot:
            return

        result = fetch(f'SELECT * FROM Levels WHERE id = "{message.author.id}"')
        xpAmount = random.randint(50, 100)

        if result:
            level = result[0][2]
            newXp = result[0][1] + xpAmount

            if result[0][1] + xpAmount >= result[0][2] * 1000 * 0.25:
                level += 1
                newXp -= result[0][2] * 1000 * 0.25

            update(f'UPDATE Levels SET xp = "{newXp}", level = "{level}" WHERE id = "{message.author.id}"')

        else:
            insert(
                f'INSERT INTO Levels (id, xp, level, design, primaryColour, secondaryColour, customBackground) VALUES (%s, %s, %s, %s, %s, %s, %s)',
                (message.author.id, 0, 1, 'dark_blue', 'ffd437', 'ffffff', 0))


def setup(client):
    client.add_cog(Levels(client))
