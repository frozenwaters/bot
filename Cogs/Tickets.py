import inspect
import random

import yaml
import discord
import sqlite3
import time
import requests

from discord.ext import commands
from Utils import permissions
from Utils.Components.Tickets import TicketDropdown
from mcstatus import MinecraftServer

server = MinecraftServer.lookup("play.frozenwaters.net")

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


class Tickets(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.persistent_views_added = False

    @commands.Cog.listener()
    async def on_ready(self):
        client = self.client

        if not self.persistent_views_added:
            client.add_view(TicketDropdown())
            self.persistent_views_added = True

    @permissions.BotAdmin()
    @commands.command(name='tickets',
                      description='Send the ticket message',
                      usage='tickets')
    async def tickets(self, context):
        client = self.client

        channel = client.get_channel(config['channels']['tickets'])
        view = TicketDropdown()
        embed = discord.Embed(
            title='Support Ticket',
            description='To open a ticket, please select one of the options from the dropdown below.',
            colour=config['embed']['colour']
        )
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])
        await channel.send(embed=embed, view=view)


def setup(client):
    client.add_cog(Tickets(client))
