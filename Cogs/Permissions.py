import inspect
import yaml
import discord
import sqlite3
import time
import requests

from discord.ext import commands
from Utils import permissions
from Utils.database import fetch, insert

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


class Permissions(commands.Cog):
    def __init__(self, client):
        self.client = client

    @permissions.BotAdmin()
    @commands.group(name='permission',
                    invoke_without_command=True,
                    usage='permission [group/user] [...]',
                    description='Permission Commands')
    async def permission(self, context):
        raise commands.MissingRequiredArgument(inspect.Parameter('UsageError', inspect.Parameter.POSITIONAL_ONLY))

    @permissions.BotAdmin()
    @permission.group(name='group',
                      invoke_without_command=True,
                      usage='permission group [add/set/remove/list] [...]',
                      description='Permission Commands')
    async def permission_group(self, context):
        raise commands.MissingRequiredArgument(inspect.Parameter('UsageError', inspect.Parameter.POSITIONAL_ONLY))

    @permissions.BotAdmin()
    @permission_group.command(name='add',
                              usage='permission group add] [role id] [permission level]',
                              description='Permission Commands')
    async def permission_group_add(self, context, roleId, permissionLevel):
        insert("INSERT INTO Permissions(id,permissionLevel) VALUES(?,?)", [roleId, permissionLevel])

        embed = discord.Embed(
            description=f'Added <@&{roleId}> with permission level `{permissionLevel}`.',
            colour=config['embed']['colour']
        )
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])
        await context.send(embed=embed)

    @permissions.BotAdmin()
    @permission_group.command(name='list',
                              usage='permission group list',
                              description='Permission Commands')
    async def permission_group_list(self, context):
        thing2 = fetch("SELECT * FROM Permissions")

        string = ''
        for i in range(0, len(thing2)):
            string += str(thing2[i]) + '\n'

        await context.send(string)


def setup(client):
    client.add_cog(Permissions(client))
