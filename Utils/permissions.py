import discord
import string
import random
import yaml
import requests

from .database import fetch, insert
from discord.ext import commands

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


def Access(permissionLevel: int = 10):
    async def predicate(ctx):
        highest = 0

        result = fetch(f'SELECT * FROM PermissionsOverride WHERE id = "{ctx.message.author.id}"')

        if result:
            if result[0][1] >= permissionLevel:
                return True
            else:
                highest = result[0][1]
                embed = discord.Embed(
                    description=f'You must have permission level `{permissionLevel}` to run this command! Your permission level is currently `{highest}`.',
                    colour=config['embed']['colour'])
                await ctx.send(embed=embed)
                return False

        result2 = fetch(f'SELECT * FROM Permissions WHERE permissionLevel >= "{permissionLevel}"')

        for i in result2:
            role = discord.utils.get(ctx.guild.roles, id=i[0])
            if role in ctx.author.roles:
                return True

        result3 = fetch(f'SELECT * FROM Permissions')

        for i in result3:
            role = discord.utils.get(ctx.guild.roles, id=i[0])
            if i[1] > highest and role in ctx.author.roles:
                highest = i[1]

        embed = discord.Embed(
            description=f'You must have permission level `{permissionLevel}` to run this command! Your permission level is currently `{highest}`.',
            colour=config['embed']['colour'])
        await ctx.send(embed=embed)
        return False

    return commands.check(predicate)


def BotAdmin():
    async def predicate(ctx):
        result = fetch(f'SELECT * FROM PermissionsOverride WHERE id = "{ctx.message.author.id}"')

        if result:
            if result[0][2] == 1:
                return True

        embed = discord.Embed(
            description='You must be a bot admin to run this command!',
            colour=config['embed']['colour'])
        await ctx.send(embed=embed)
        return False

    return commands.check(predicate)


def AccessCheck(ctx, permissionLevel: int = 10):
    highest = 0

    result = fetch(f'SELECT * FROM PermissionsOverride WHERE id = "{ctx.message.author.id}"')

    if result:
        if result[0][1] >= permissionLevel:
            return True
        else:
            return False

    result2 = fetch(f'SELECT * FROM Permissions WHERE permissionLevel >= "{permissionLevel}"')

    for i in result2:
        role = discord.utils.get(ctx.guild.roles, id=i[0])
        if role in ctx.author.roles:
            return True

    return False


def BotAdminCheck(ctx):
    result = fetch(f'SELECT * FROM PermissionsOverride WHERE id = "{ctx.message.author.id}"')

    if result:
        if result[0][2] == 1:
            return True
    return False


# Redundant not updated code
#
# def getPermissionLevel(user, guild):
#     mycursor.execute(f'SELECT * FROM PermissionOverride WHERE id = "{user.id}"')
#     result = mycursor.fetchall()
#
#     if result:
#         return result[0][1]
#
#     mycursor.execute(f'SELECT * FROM UserPermissions WHERE guildId = "{guild.id}" AND id = "{user.id}"')
#     result2 = mycursor.fetchall()
#
#     if result2:
#         return result2[0][1]
#
#     mycursor.execute(f'SELECT * FROM RolePermissions WHERE guildId = "{guild.id}"')
#     result3 = mycursor.fetchall()
#
#     for i in result3:
#         role = discord.utils.get(guild.roles, id=i[0])
#         if i[1] > highest and role in user.roles:
#             highest = i[1]
#
#     return highest
#
#
# def getBotAdmin(user):
#     mycursor.execute(f'SELECT * FROM PermissionOverride WHERE id = "{user.id}"')
#     result = mycursor.fetchall()
#
#     if result:
#         return True
#     else:
#         return False
