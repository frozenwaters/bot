import time

from PIL import Image, ImageDraw, ImageFont, ImageOps
from .database import fetch, insert, update

colors = {
    'dark_blue': {'c': (27, 53, 81), 'p_font': 'rgb(255,255,255)', 's_font': 'rgb(255, 212, 55)'},
    'grey': {'c': (70, 86, 95), 'p_font': 'rgb(255,255,255)', 's_font': 'rgb(93,188,210)'},
    'light_blue': {'c': (93, 188, 210), 'p_font': 'rgb(27,53,81)', 's_font': 'rgb(255,255,255)'},
    'blue': {'c': (23, 114, 237), 'p_font': 'rgb(255,255,255)', 's_font': 'rgb(255, 255, 255)'},
    'orange': {'c': (242, 174, 100), 'p_font': 'rgb(0,0,0)', 's_font': 'rgb(0,0,0)'},
    'purple': {'c': (114, 88, 136), 'p_font': 'rgb(255,255,255)', 's_font': 'rgb(255, 212, 55)'},
    'red': {'c': (255, 0, 0), 'p_font': 'rgb(0,0,0)', 's_font': 'rgb(0,0,0)'},
    'yellow': {'c': (255, 255, 0), 'p_font': 'rgb(0,0,0)', 's_font': 'rgb(27,53,81)'},
    'yellow_green': {'c': (232, 240, 165), 'p_font': 'rgb(0,0,0)', 's_font': 'rgb(0,0,0)'},
    'green': {'c': (65, 162, 77), 'p_font': 'rgb(217, 210, 192)', 's_font': 'rgb(0, 0, 0)'}
}

x, y = 250, 150
size_x, size_y = 600, 50


def add_color(image, c, transparency):
    color = Image.new('RGB', image.size, c)
    mask = Image.new('RGBA', image.size, (0, 0, 0, transparency))
    return Image.composite(image, color, mask).convert('RGB')


def add_text(background, color, text, font, font_size, position):
    draw = ImageDraw.Draw(background)

    font = ImageFont.truetype(font, font_size)
    draw.text(position, text, fill=color, font=font, anchor='lb')
    return background


def add_text2(background, color, text, font, font_size, position):
    draw = ImageDraw.Draw(background)

    font = ImageFont.truetype(font, font_size)
    draw.text(position, text, fill=color, font=font, anchor='rt')
    return background


def add_xp(background, color, font, xp, total, level, rank, ranks):
    draw = ImageDraw.Draw(background)

    draw.ellipse((x, y, x + size_y, y + size_y), fill=color['p_font'])
    draw.ellipse((x + size_x, y, x + size_x + size_y, y + size_y), fill=color['p_font'])
    draw.rectangle((x + (size_y / 2), y, x + size_x + (size_y / 2), y + size_y), fill=color['p_font'])

    draw.ellipse((x, y, x + size_y, y + size_y), fill=color['s_font'])

    percentage = xp / total
    value = (1 - percentage) / 3
    length = (x + size_x) * (percentage + value)

    draw.ellipse((length, y, length + size_y, y + size_y), fill=color['s_font'])
    draw.rectangle((x + (size_y / 2), y, length + (size_y / 2), y + size_y), fill=color['s_font'])

    font = ImageFont.truetype(font, 25)

    draw.text((x + size_x + size_y, y + size_y + 10), f'Level {level}      -      Rank # {rank} / {len(ranks)}      -      {xp} / {int(total)}', anchor='rt',
              fill=color['s_font'], font=font)
    draw.text((x + size_x + size_y, y + size_y + 10), f'/ {int(total)}', anchor='rt', fill=color['p_font'], font=font)
    return background


def add_image(background, image, position):
    bg_w, bg_h = background.size
    img_w, img_h = image.size
    background.paste(image, position, image)
    return background


def add_corners(im, rad):
    circle = Image.new('L', (rad * 2, rad * 2), 0)
    draw = ImageDraw.Draw(circle)
    draw.ellipse((0, 0, rad * 2, rad * 2), fill=255)
    alpha = Image.new('L', im.size, "white")
    w, h = im.size
    alpha.paste(circle.crop((0, 0, rad, rad)), (0, 0))
    alpha.paste(circle.crop((0, rad, rad, rad * 2)), (0, h - rad))
    alpha.paste(circle.crop((rad, 0, rad * 2, rad)), (w - rad, 0))
    alpha.paste(circle.crop((rad, rad, rad * 2, rad * 2)), (w - rad, h - rad))
    im.putalpha(alpha)
    return im


def generateImage(user):
    # create Image object
    result = fetch(f'SELECT * FROM Levels WHERE id = "{user.id}"')

    if not result:
        time.sleep(2)
        result = fetch(f'SELECT * FROM Levels WHERE id = "{user.id}"')
        if not result:
            return False

    rank = '?'
    result2 = fetch(f'SELECT * FROM Levels ORDER BY level DESC, xp DESC')
    for i, item in enumerate(result2):
        if int(item[0]) == user.id:
            rank = i + 1

    color = colors[result[0][3]]  # grey,light_blue,blue,orange,purple,yellow,green
    font_name = 'Assets/Rubik-SemiBold.ttf'

    background = Image.open('Assets/background.png').convert("RGBA")
    pfp = Image.open(f'Assets/Profiles/{user.id}.png').convert("RGBA")

    status = Image.open(f'Assets/{user.status}.png').convert("RGBA")
    crown = Image.open(f'Assets/crown.png').convert("RGBA")

    bigsize = (pfp.size[0] * 3, pfp.size[1] * 3)
    mask = Image.new('L', bigsize, 0)
    draw = ImageDraw.Draw(mask)
    draw.ellipse((0, 0) + bigsize, fill=255)
    draw.ellipse((pfp.size[0] * 3 * 0.7031, pfp.size[1] * 3 * 0.6689, pfp.size[0] * 0.9960 * 3, pfp.size[1] * 3 * 0.9619), fill=0)
    mask = mask.resize(pfp.size, Image.ANTIALIAS)
    pfp.putalpha(mask)

    pfp = pfp.resize((190, 190), Image.ANTIALIAS)
    status.thumbnail((64, 64), Image.ANTIALIAS)
    crown.thumbnail((150, 150), Image.ANTIALIAS)

    background = add_color(background, color['c'], 40)

    font_size = 200
    font = ImageFont.truetype(font_name, font_size)

    while font.getsize(f'{user.name}#{user.discriminator}')[0] > size_x or \
            font.getsize(f'{user.name}#{user.discriminator}')[1] > size_y * 1.25:
        font_size -= 1

        if font_size == 0:
            return False

        font = ImageFont.truetype(font_name, font_size)

    add_text(background, color['p_font'], f'{user.name}#{user.discriminator}', font_name, font_size, (x, y - 10))
    add_text(background, color['s_font'], f'{user.name}', font_name, font_size, (x, y - 10))
    add_image(background, pfp, (40, 46))
    add_image(background, status, (170, 170))
    
    if rank == 1:
        add_image(background, crown, (20, -20))

    add_xp(background, color, font_name, result[0][1], result[0][2] * 1000 * 0.25, result[0][2], rank, result2)
    add_corners(background, 25)
    background.save(f'Assets/Cards/{user.id}.png')
    return True
