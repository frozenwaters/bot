import time

import discord
import yaml

from Utils.database import insert, update, fetch, insertId

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile, Loader=yaml.FullLoader)

with open("bot.yml", "r") as ymlfile:
    bot = yaml.load(ymlfile, Loader=yaml.FullLoader)


class TicketSelect(discord.ui.Select):
    def __init__(self, ):
        options = [
            discord.SelectOption(label='Server Bug / Issue',
                                 description='Report any bugs or issues with the minecraft server.', emoji='🐛'),
            discord.SelectOption(label='Player Report', description='Report a players actions (discord or server).',
                                 emoji='🔒'),
            discord.SelectOption(label='Support', description='Get help with anything Frozenwaters related.',
                                 emoji='❓'),
            discord.SelectOption(label='Other', description='Anything which doesnt fall into an above category.',
                                 emoji='📰')
        ]

        super().__init__(placeholder='Select a reason...', min_values=1, max_values=1, options=options,
                         custom_id='persistent_ticket_dropdown')

    async def callback(self, interaction: discord.Interaction):
        result = insertId(f'INSERT INTO Tickets (authorId, reason) VALUES (%s, %s)',
                          (interaction.user.id, self.values[0]))

        catgeory = discord.utils.get(interaction.guild.categories, id=config['categories']['tickets'])
        staff = discord.utils.get(interaction.guild.roles, id=config['roles']['staff'])

        overwrites = {
            interaction.user: discord.PermissionOverwrite(read_messages=True),
            interaction.guild.default_role: discord.PermissionOverwrite(read_messages=False),
            staff: discord.PermissionOverwrite(read_messages=True)

        }
        ticket = await interaction.guild.create_text_channel(name=f'ticket-{str(result).zfill(4)}', category=catgeory,
                                                             topic=self.values[0], overwrites=overwrites)
        embed = discord.Embed(
            title='Support Ticket',
            description=f'Hello <@{interaction.user.id}>,\n\nThank you for contacting us!\n\nWe will get back to you as soon as possible, in the mean time please describe your query thoroughly.',
            colour=config['embed']['colour']
        )
        embed.add_field(name='Reason', value=self.values[0], inline=False)
        embed.set_footer(text=config['embed']['footer']['text'], icon_url=config['embed']['footer']['url'])

        await ticket.send(embed=embed)
        await interaction.response.send_message(f'A ticket has been opened, you can access it here: <#{ticket.id}>',
                                                ephemeral=True)


class TicketDropdown(discord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)

        # Adds the dropdown to our view object.
        self.add_item(TicketSelect())
